//
//  ResourceDetailTableViewController.swift
//  LearningSwift
//
//  Created by Lorenzo Rheinicke on 2015/08/12.
//  Copyright (c) 2015 LCR Technologies. All rights reserved.
//

import UIKit

class ResourceDetailTableViewController: UITableViewController {
    
    var resource:Resource?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var resourceTypeLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = resource?.title
        titleLabel.textColor = UIColor.redColor()
        
        resourceTypeLabel.text = resource?.resourceType
        println("url: \(resource?.url)")
        urlLabel.text = resource?.url
        descriptionLabel.text = resource?.description
        
        println("resource description = \(resource?.description)")
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

   override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
        
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "URL Segue" {
            let webVC = segue.destinationViewController as! WebViewController
            webVC.webUrl = resource?.url
            webVC.title = resource?.title
        }
        
    }
}
