//
//  Resource.swift
//  LearningSwift
//
//  Created by Lorenzo Rheinicke on 2015/08/12.
//  Copyright (c) 2015 LCR Technologies. All rights reserved.
//

import Foundation

class Resource {
    let id:Int
    let title:String
    let description:String
    var url:String?
    var resourceType:String?

    init(id:Int, title:String, description:String){
        self.id = id
        self.title = title
        self.description = description
    }
}
