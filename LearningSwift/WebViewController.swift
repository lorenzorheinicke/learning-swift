//
//  WebViewController.swift
//  LearningSwift
//
//  Created by Lorenzo Rheinicke on 2015/08/12.
//  Copyright (c) 2015 LCR Technologies. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {
    
    var webUrl:String?

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let urlStr = webUrl {
            let url = NSURL(string: urlStr)
            let urlRequest = NSURLRequest(URL: url!)
            webView.loadRequest(urlRequest)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
